drop database if exists tehtavalista;
create database tehtavalista;
use tehtavalista;

CREATE TABLE task (
    id INT auto_increment PRIMARY KEY,
    description VARCHAR(255) NOT NULL,
    done boolean default false,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);